﻿using UnityEngine;
using System.Collections;

public class sphereControllerScript : MonoBehaviour
{

	// Global (no pun intended) settings
	bool CanZoomIn;
	bool CanZoomOut;
	byte twoFingerAction;
	const byte MaskLayer = 8;
	// Sensitivity of zoom relative to rotation
	// As sensitivity goes to 45, 
	// zoom angle => rotate angle
	const uint SensitivityWeight = 40;

	float LongitudinalRotate;
	float LateralRotate;

	static uint LowerPosAngle;
	static uint UpperPosAngle;
	static uint LowerNegAngle;
	static uint UpperNegAngle;

	float LeftTouchAngle;
	float RightTouchAngle;

	float SqrdCamDistance;
	float MaxZoom;
	float MinZoom;
	float MaxZoomDistanceSqrd;
	float MinZoomDistanceSqrd;
	float ClampDistance;

	float RotationAmount;
	float ZRotationAmount;
	float ZoomAmount;

	Ray FirstFingerRay;
	Ray SecondFingerRay;

	RaycastHit RayHit;

	Touch FirstTouch;
	Touch SecondTouch;
	Touch Left_touch;
	Touch Right_touch;

	Vector2 LeftFingerVector;
	Vector2 RightFingerVector;
	Vector2 LeftTouchDrag;
	Vector2 RightTouchDrag;
	Vector2 FingerDirVector;
	Vector3 CamVertAxis;
	Vector3 CamRightAxis;
	Vector3 CamFrontAxis;
	Vector3 CameraPosition;

	LayerMask Mask;

	Camera MainCamera;

	Transform TargetItem;
	Transform MainCameraTransform;

	// Use this for initialization
	void Start ()
	{
		CanZoomIn = true;
		CanZoomOut = true;
		MaxZoom = 0.65f;
		MinZoom = 0.9f;
		MaxZoomDistanceSqrd = - (MaxZoom * MaxZoom);
		MinZoomDistanceSqrd = - (MinZoom * MinZoom);
		
		LowerPosAngle = 360 - SensitivityWeight;
		UpperPosAngle = SensitivityWeight;
		LowerNegAngle = 180 - SensitivityWeight;
		UpperNegAngle = 180 + SensitivityWeight;
		
		RotationAmount = 0.1f;
		ZRotationAmount = 0.53f;
		ZoomAmount = 0.005f;
		
		Mask = 1 << MaskLayer;

		MainCamera = Camera.main;

		TargetItem = GameObject.FindWithTag ("WorldTag").transform;
		MainCameraTransform = GameObject.FindWithTag ("MainCamera").transform;
		
		FingerDirVector = new Vector2 (0, 0);
	}
	
	// Update is called once per frame
	void Update ()
	{	
		// Need to be called at each frame to ensure
		// the proper camera orientation
		CamVertAxis = MainCameraTransform.up;
		CamRightAxis = MainCameraTransform.right;
		CamFrontAxis = MainCameraTransform.forward;
		CameraPosition = MainCameraTransform.position;

		if (Input.touchCount > 0) {
			// Test if there was a touch this frame
			if (Input.touchCount == 1) {
				// Pan the World model
				Touch touch = Input.GetTouch (0);

				// Check if the touch was made on the world model
				FirstFingerRay = MainCamera.ScreenPointToRay (touch.position);
				if (Physics.Raycast (FirstFingerRay, out RayHit, 10.0f, Mask)) {
					if (touch.phase == TouchPhase.Moved) {

						// Debug.Log ("World Moved!");

						// This rotation method is the realistic
						// "click and drag body" method!
						LongitudinalRotate = 
							touch.deltaPosition.x * RotationAmount;
						LateralRotate = 
							touch.deltaPosition.y * -RotationAmount;

						// Perform the rotation
						MainCameraTransform.RotateAround (
							Vector3.zero,
							CamRightAxis,
							LateralRotate);
						MainCameraTransform.RotateAround (
							Vector3.zero,
							CamVertAxis,
							LongitudinalRotate);
					}
				}

			} else if (Input.touchCount == 2) {
				// Pan around z-axis or zoom
				// Debug.Log ("Detecting two touches...");
				// Assumes first touch will be lowest on y-scale
				FirstTouch = Input.GetTouch (0);
				SecondTouch = Input.GetTouch (1);

				// Define finger rays
				FirstFingerRay = MainCamera.ScreenPointToRay (
					FirstTouch.position);
				SecondFingerRay = MainCamera.ScreenPointToRay (
					SecondTouch.position);

				// Check if the touches were applied in the world model
				if (Physics.Raycast (
					FirstFingerRay, out RayHit, 10.0f, Mask) ||
					Physics.Raycast (
					SecondFingerRay, out RayHit, 10.0f, Mask)) {

					// Make touches absolute - apply left-most touch
					// to touch_one and right-most touch to touch_two
					Left_touch = FirstTouch; 
					// Assumes first touch
					// is already left-most touch
					Right_touch = SecondTouch; 
					// Assumes second touch
					// is already right-most touch

					if (FirstTouch.position.x > SecondTouch.position.x) {
						// First touch is right-most
						// This reverses the assumption in the
						// initiation above if true!
						Left_touch = SecondTouch;
						Right_touch = FirstTouch;
						
					} // This ensures that the fingerDirVector always
					// points from left to right!

					// Check if this is the initial touch
					if (Left_touch.phase == TouchPhase.Began ||
						Right_touch.phase == TouchPhase.Began) {

						// Define the finger positions as vectors
						LeftFingerVector = Left_touch.position;
						RightFingerVector = Right_touch.position;

						// Get the finger difference vector
						// Should always have (+) x-component!
						FingerDirVector = 
							(RightFingerVector + (-LeftFingerVector));

						// Check if this touch is a drag
					} else if (Left_touch.phase == TouchPhase.Moved ||
						Right_touch.phase == TouchPhase.Moved) {
						// Debug.Log ("Fingers are moving...");

						// Debug portion to ensure fingerDirVector is
						// being handled correctly

						/* float fingerDirAngle = 
							VectorHelperFunctions.RelAngleToXAxis (
								fingerDirVector);
								
						Debug.Log (fingerDirAngle);
						
						if (!((fingerDirAngle > 0 && 
							fingerDirAngle < 90) ||
							(fingerDirAngle > 270 && 
							fingerDirAngle < 360))) {
							Debug.Log ("Something went wrong!");
						} */

						// Set the vectors of the finger drag
						LeftTouchDrag = 
							Left_touch.deltaPosition;
						RightTouchDrag = 
							Right_touch.deltaPosition;

						// Get correct, full 360-degree orientation
						// of finger delta vector relative to
						// finger direction vector
						LeftTouchAngle = 
							AbsoluteAngle (LeftTouchDrag, FingerDirVector);
						RightTouchAngle =
							AbsoluteAngle (RightTouchDrag, FingerDirVector);

						// Get distance from camera to origin
						SqrdCamDistance = 
							- (CameraPosition.sqrMagnitude);

						// Is less than maximum zoom / can zoom in and out:
						CanZoomIn = SqrdCamDistance < MaxZoomDistanceSqrd;
						// Is more than minimum zoom / can zoom in and out:
						CanZoomOut = SqrdCamDistance > MinZoomDistanceSqrd;
						/* Debug.Log (MinZoomDistanceSqrd + "<" + 
							SqrdCamDistance + "<" + MaxZoomDistanceSqrd); */

						// Better optimized implementation
						twoFingerAction = TwoFingerAction (
							LeftTouchAngle, RightTouchAngle);
						// Debug.Log (twoFingerAction);

						switch (twoFingerAction) {
						case 1:
							// Rotate CCW
							MainCameraTransform.RotateAround (
										TargetItem.position,
										CamFrontAxis,
										-ZRotationAmount);
							break;

						case 2:
							// Rotate CW
							MainCameraTransform.RotateAround (
										TargetItem.position,
										CamFrontAxis,
										ZRotationAmount);
							break;

						case 3:
							switch (CanZoomIn) {
							case true:
								// Zoom In
								MainCameraTransform.Translate (
									0, 0, ZoomAmount);
								break;
							}
							break;

						case 4:
							switch (CanZoomOut) {
							case true:
								// Zoom Out
								MainCameraTransform.Translate (
									0, 0, -ZoomAmount);
								break;
							}
							break;
						}
					}
				}
			}
		}
	}

	static float AbsoluteAngle (Vector2 vectorFrom, Vector2 vectorTo)
	{
		// Returns the absolute angle between two vectors
		// This accounts for full 360 rotation
		
		float returnAngle = 0.0f;
		float rigidAngle = Vector2.Angle (vectorFrom, vectorTo);
		Vector3 crossSign = Vector3.Cross (vectorFrom, vectorTo);
		
		if (crossSign.z > 0) {
			returnAngle = 360 - rigidAngle;
		} else if (crossSign.z < 0) {
			// Something else?
			// For now:
			returnAngle = rigidAngle;
		}
		
		return returnAngle;
		
	}

	static byte TwoFingerAction (float LeftTouchAngle, float RightTouchAngle)
	{
		// Debug.Log ("Entering TwoFingerAction...");

		byte returnAction = 0;
		
		if ((UpperNegAngle < LeftTouchAngle) &&
			(LeftTouchAngle < LowerPosAngle) &&
			(UpperPosAngle < RightTouchAngle) &&
			(RightTouchAngle < LowerNegAngle)) {
			// Rotate CCW
			returnAction = 1;
			
		} else if ((UpperPosAngle < LeftTouchAngle) &&
			(LeftTouchAngle < LowerNegAngle) && 
			(UpperNegAngle < RightTouchAngle) && 
			(RightTouchAngle < LowerPosAngle)) {
			// Rotate CW
			returnAction = 2;
			
		} else if ((LowerNegAngle < LeftTouchAngle) &&
			(LeftTouchAngle < UpperNegAngle) &&
			((LowerPosAngle < RightTouchAngle) ||
			(RightTouchAngle < UpperPosAngle))) {
			// Zoom in
			returnAction = 3;
			
		} else if (((LowerPosAngle < LeftTouchAngle) ||
			(LeftTouchAngle < UpperPosAngle)) &&
			(LowerNegAngle < RightTouchAngle) &&
			(RightTouchAngle < UpperNegAngle)) {
			// Zoom out
			returnAction = 4;
		}
		
		return returnAction;

	}
}
